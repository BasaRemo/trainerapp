### What is this App for? ###
The app reproduces a boxing round and other boxing trainings like speeds training where you have for example 1 seconds to throw as many punches as possible. 

### TODO:  ###
* Add list to manage differents type of trainings
* Add basic default trainings: 
    - 1 second punches + 10 seconds break
    - 1 second punches +  random break time
    - Boxing round 

* Add audio sounds: Go!,Stop!,Break! Time is Up! and other basic sounds to improve the experience.

* Add motivation audio for break times:
    *  Mike Tyson, INTIMIDATION : http://www.youtube.com/watch?v=S9MtJ164XJI
    *  FootBall Pregame Speech! http://www.youtube.com/watch?v=E3xbdaGD4dk 
    *  The privilege that I have to know you by name! http://www.youtube.com/watch?v=cK5cTFHc_9E
    *  It's a man's game tonight! http://www.youtube.com/watch?v=sDUmVprZ43M
    * We planned for this! http://www.youtube.com/watch?v=5TpfOY_JwSg
    * My everything or Nothing! http://www.youtube.com/watch?v=-Nnstylh2g8

> Note to self: If you don't get pumped up listening to these..Hopeless..

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

![secondScreen.png](https://bitbucket.org/repo/zo4zXq/images/1176640683-secondScreen.png)![firstScreen.png](https://bitbucket.org/repo/zo4zXq/images/1149045472-firstScreen.png)

### Who do I talk to? ###

* Ntambwa Remo Basambombo : 
    * basadev4@gmail.com
    * https://www.linkedin.com/pub/ntambwa-basambombo/52/1b9/137