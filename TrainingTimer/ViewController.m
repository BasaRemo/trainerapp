//
//  ViewController.m
//  TrainingTimer
//
//  Created by Ntambwa Basambombo on 2014-09-20.
//  Copyright (c) 2014 Basa. All rights reserved.
//

#import "ViewController.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface ViewController ()

@end

@implementation ViewController
@synthesize resetBtn;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _counterViewSecond.delegate = self;
    [self initTime];
    [self setAppareance];
    [self setCustomSupport];
    

}

- (void)initTime
{

    NSLog (@"nbRepeat: %d  ", _nbRepeat);
    NSLog (@"_exerciceTimeDuration: %d ms", _exerciceTimeDuration);
    NSLog (@"_breakTimeDuration: %d  ms", _breakTimeDuration);
    
    NSNumber* workTime = [NSNumber numberWithInt:_exerciceTimeDuration];
    NSNumber* breakTime = [NSNumber numberWithInt:_breakTimeDuration];
    
    self.counterViewSecond.intervals=[[NSMutableArray alloc] initWithObjects:breakTime, nil];
    self.counterViewSecond.intervals=[[NSMutableArray alloc] initWithObjects:workTime, nil];
    for(int i=0;i<_nbRepeat-1;i++){
        [self.counterViewSecond.intervals addObject:breakTime];
        [self.counterViewSecond.intervals addObject:workTime];
 
    }
    
}

- (void)setAppareance
{
    // Start Stop Btn
    [self.startStopButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.startStopButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.startStopButton.tapCircleColor = UIColorFromRGB(0x3794c3);  // Setting this color overrides "Smart Color".
    self.startStopButton.cornerRadius = self.startStopButton.frame.size.width/2;
    self.startStopButton.backgroundColor = UIColorFromRGB(0x50a1cb);
    
    self.startStopButton.rippleFromTapLocation = NO;
    self.startStopButton.rippleBeyondBounds = YES;
    self.startStopButton.tapCircleDiameter = MAX(self.startStopButton.frame.size.width, self.startStopButton.frame.size.height) * 1.3;
    
    //Reset Btn
    [self.resetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.resetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.resetBtn.tapCircleColor = UIColorFromRGB(0xb4d7e8);  // Setting this color overrides "Smart Color".
    self.resetBtn.cornerRadius = self.resetBtn.frame.size.width/2;
    self.resetBtn.backgroundColor = UIColorFromRGB(0x9bc9e1);
    
    self.resetBtn.rippleFromTapLocation = NO;
    self.resetBtn.rippleBeyondBounds = YES;
    self.resetBtn.tapCircleDiameter = MAX(self.resetBtn.frame.size.width, self.resetBtn.frame.size.height) * 1.3;
    
    // thickness of outer circle
    self.counterViewSecond.outerCircleThickness = [NSNumber numberWithFloat:15.0];
    
    // thickness of inner circle
    self.counterViewSecond.innerCircleThickness = [NSNumber numberWithFloat:5.0];
    
    // track color of outer circle
    //self.counterViewSecond.innerTrackColor = [UIColor redColor];
    
    // track color of inner circle
    //self.counterViewSecond.outerTrackColor = [UIColor blackColor];
    
    // distance between two circles (if multiple intervals)
    self.counterViewSecond.circleDistance = [NSNumber numberWithFloat:4.0];
    
    // set color of outer progress circles
    self.counterViewSecond.outerProgressColor = [UIColor whiteColor];
    
    // set color of inner progress circle
    self.counterViewSecond.innerProgressColor = [UIColor orangeColor];
    
    // set color of counter label
    self.counterViewSecond.labelColor = [UIColor orangeColor];
    
    // set view background color
    self.counterViewSecond.backgroundColor = UIColorFromRGB(0x067AB5);
    
    // hide fraction 
    //self.counterViewSecond.hideFraction = YES;
}

-(void) setCustomSupport{
    // Set this up once when your application launches
    UVConfig *config = [UVConfig configWithSite:@"basa.uservoice.com"];
    config.forumId = 266460;
    // [config identifyUserWithEmail:@"email@example.com" name:@"User Name", guid:@"USER_ID");
    [UserVoice initialize:config];
    
    // Call this wherever you want to launch UserVoice
    //[UserVoice presentUserVoiceInterfaceForParentViewController:self];
}

- (void)playEndRoundSound
{
    [[AFSoundManager sharedManager]startPlayingLocalFileWithName:@"doublebeep.mp3" andBlock:nil];
}
- (void)playEndSerieSound
{
}
- (void)playStartSerieSound
{
}
- (void)play30SecLeftSound
{
}

- (IBAction)startCountdown:(UIButton *)sender {
    
        // start
        if ([self.startStopButton.currentTitle isEqualToString:@"START"]) {
            
            [self.counterViewSecond start];
            [self.startStopButton setTitle:@"STOP" forState:UIControlStateNormal];
            // stop
        } else if ([self.startStopButton.currentTitle isEqualToString:@"STOP"]) {
            
            [self.counterViewSecond stop];
            [self.startStopButton setTitle:@"RESUME" forState:UIControlStateNormal];
            // resume
        } else {
            
            [self.counterViewSecond resume];
            [self.startStopButton setTitle:@"STOP" forState:UIControlStateNormal];
        }
    
}

- (void)intervalDidEnd:(SFRoundProgressCounterView*)progressCounterView WithIntervalPosition:(int)position{

    //NSLog(@"position = %i", position);
    [self playEndRoundSound];
    
}

- (IBAction)resetTapped:(id)sender {
}

- (void)countdownDidEnd:(SFRoundProgressCounterView*)progressCounterView{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.startStopButton setTitle:@"START" forState:UIControlStateNormal];
                //[self.counterViewSecond reset];
    });
    
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"SERIE END!"
                                                   description:@"GOOOD JOB!! You have finished your series"
                                                          type:TWMessageBarMessageTypeSuccess
                                                      duration:5.0];
}

- (void)counter:(SFRoundProgressCounterView *)progressCounterView didReachValue:(unsigned long long)value{
    //NSLog(@"position = %llu", value/60);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
