//
//  TimeSelectionViewController.m
//  TrainingTimer
//
//  Created by Ntambwa Basambombo on 2014-09-21.
//  Copyright (c) 2014 Basa. All rights reserved.
//

#import "TimeSelectionViewController.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

const static CGFloat kJVFieldHeight = 44.0f;
const static CGFloat kJVFieldHMargin = 10.0f;
const static CGFloat kJVFieldFontSize = 16.0f;
const static CGFloat kJVFieldFloatingLabelFontSize = 11.0f;

@interface TimeSelectionViewController ()
{
    NSMutableArray *arrayOfHour;
    NSMutableArray *arrayOfMinute;
    NSMutableArray *arrayOfSecond;
    NSMutableArray *arrayOfMilliSecond;
}
@property (strong,nonatomic) NSArray *theData;
@end

@implementation TimeSelectionViewController
@synthesize picker,roundDuration,breakDuration,roundNb;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNbPad];
    [self createCustomTimePicker];
    [self setLabelStyle];
    [self setBtnAppareance];
    [self initArrays];
    
}

-(void) initArrays {
    arrayOfHour = [[NSMutableArray alloc] initWithCapacity:10];
    arrayOfMinute = [[NSMutableArray alloc] initWithCapacity:60];
    arrayOfSecond = [[NSMutableArray alloc] initWithCapacity:60];
    arrayOfMilliSecond = [[NSMutableArray alloc] initWithCapacity:10];
    
    for(int i=0;i<=9;i++){
        [arrayOfHour addObject:[NSNumber numberWithInt:i]];
        [arrayOfMilliSecond addObject:[NSNumber numberWithInt:i]];
    }
    for(int i=0;i<=59;i++){
        [arrayOfSecond addObject:[NSNumber numberWithInt:i]];
        [arrayOfMinute addObject:[NSNumber numberWithInt:i]];
        
    }
    
}

- (void)setBtnAppareance
{
    //Nav Bar appearance
    [self.navigationController.navigationBar  setBarTintColor:UIColorFromRGB(0x067AB5)];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem setTitle:@"Training Timer"];
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:nil];
    //UIBarButtonItem *cameraItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemUndo target:self action:nil];
    
    //NSArray *actionButtonItems = @[shareItem, cameraItem];
    self.navigationItem.leftBarButtonItem = shareItem;
    //self.navigationItem.rightBarButtonItem = cameraItem;
    
    self.startTrainingButton.buttonColor = UIColorFromRGB(0x067AB5);
    self.startTrainingButton.shadowColor = UIColorFromRGB(0x067AC6);
    self.startTrainingButton.shadowHeight = 3.0f;
    self.startTrainingButton.cornerRadius = 6.0f;
    self.startTrainingButton.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [self.startTrainingButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.startTrainingButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];
}

-(void) setNbPad{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked:)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done_clicked:)];
    numberToolbar.items = [NSArray arrayWithObjects: cancelBtn, space,doneBtn,nil];
    
    [numberToolbar sizeToFit];
    roundNb.inputAccessoryView = numberToolbar;
}

// Set the form labels and design
-(void) setLabelStyle {
    UIColor *floatingLabelColor = [UIColor brownColor];
    
    roundDuration.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Round Lenght", @"")
                                                                       attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    roundDuration.font = [UIFont systemFontOfSize:kJVFieldFontSize];
    roundDuration.floatingLabel.font = [UIFont boldSystemFontOfSize:kJVFieldFloatingLabelFontSize];
    roundDuration.floatingLabelTextColor = floatingLabelColor;
    roundDuration.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    breakDuration.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Break Lenght", @"")
                                                                          attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    breakDuration.font = [UIFont systemFontOfSize:kJVFieldFontSize];
    breakDuration.floatingLabel.font = [UIFont boldSystemFontOfSize:kJVFieldFloatingLabelFontSize];
    breakDuration.floatingLabelTextColor = floatingLabelColor;
    breakDuration.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    roundNb.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"repeat", @"")
                                                                          attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    roundNb.font = [UIFont systemFontOfSize:kJVFieldFontSize];
    roundNb.floatingLabel.font = [UIFont boldSystemFontOfSize:kJVFieldFloatingLabelFontSize];
    roundNb.floatingLabelTextColor = floatingLabelColor;
    roundNb.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [self.separatorView setTopLineSeparatorType:VENSeparatorTypeStraight
                        bottomLineSeparatorType:VENSeparatorTypeStraight];
    self.separatorView.fillColor         = self.view.backgroundColor;
    self.separatorView.topStrokeColor    = [UIColor blackColor];
    self.separatorView.bottomStrokeColor = [UIColor blackColor];
    
    [self.separatorView3 setTopLineSeparatorType:VENSeparatorTypeJagged
                         bottomLineSeparatorType:VENSeparatorTypeJagged];
    self.separatorView3.fillColor         = self.view.backgroundColor;
    self.separatorView3.topStrokeColor    = [UIColor blackColor];
    self.separatorView3.bottomStrokeColor = [UIColor blackColor];
    
    [self.separatorView4 setTopLineSeparatorType:VENSeparatorTypeNone
                         bottomLineSeparatorType:VENSeparatorTypeJagged];
    self.separatorView4.fillColor         = self.view.backgroundColor;
    self.separatorView4.topStrokeColor    = [UIColor blackColor];
    self.separatorView4.bottomStrokeColor = [UIColor blackColor];
}

//Create the time Picker with hours, min , sec and ms
-(void) createCustomTimePicker{

    picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width, 200)];
    picker.dataSource = self;
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UILabel *hourLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, picker.frame.size.height / 2 - 15, 70, 30)];
    hourLabel.text = @"hour";
    [picker addSubview:hourLabel];
    
    UILabel *minsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0 + (picker.frame.size.width / 3), picker.frame.size.height / 2 - 15, 75, 30)];
    minsLabel.text = @"min";
    [picker addSubview:minsLabel];
    
    UILabel *secsLabel = [[UILabel alloc] initWithFrame:CGRectMake(-25 + ((picker.frame.size.width / 3) * 2), picker.frame.size.height / 2 - 15, 75, 30)];
    secsLabel.text = @"sec";
    [picker addSubview:secsLabel];
    
    UILabel *millSecLabel = [[UILabel alloc] initWithFrame:CGRectMake(52 + ((picker.frame.size.width / 3) * 2), picker.frame.size.height / 2 - 15, 75, 30)];
    millSecLabel.text = @"ms";
    [picker addSubview:millSecLabel];
    
    UIToolbar* selectTimeBar = [[UIToolbar alloc] initWithFrame:CGRectMake(5, 0, 320, 44)];
    //[selectTimeBar setBarStyle:UIBarStyleBlackTranslucent];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked:)];

    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done_clicked:)];
    
    [selectTimeBar setItems: [NSArray arrayWithObjects:cancelBtn,space, doneBtn, nil]];
    
    [roundDuration setInputView:picker];
    [roundDuration setInputAccessoryView:selectTimeBar];
    
    [breakDuration setInputView:picker];
    [breakDuration setInputAccessoryView:selectTimeBar];

}


- (IBAction)durationFieldSelected:(JVFloatLabeledTextField *)sender {
    
    NSLog(@"User selected %@", sender.text);
    int hRow = [[arrayOfHour objectAtIndex:[picker selectedRowInComponent:0]] intValue];
    int mRow = [[arrayOfMinute objectAtIndex:[picker selectedRowInComponent:1]] intValue];
    int sRow = [[arrayOfSecond objectAtIndex:[picker selectedRowInComponent:2]] intValue];
    int msRow = [[arrayOfMilliSecond objectAtIndex:[picker selectedRowInComponent:3]] intValue];
    NSString* unit;
    NSString* durationTime;
    
    if (hRow ==0) {
        unit = @"minutes(s)";
        durationTime = [NSString stringWithFormat:@"%d : %d : %d %@", mRow , sRow, msRow, unit];
    }else{
        unit = @"hour(s)";
        durationTime = [NSString stringWithFormat:@"%d : %d : %d : %d %@",hRow, mRow , sRow, msRow, unit];
    }
    if (hRow ==0 && mRow ==0) {
        unit = @"second(s)";
        durationTime = [NSString stringWithFormat:@"%d : %d %@",sRow, msRow, unit];
    }
    if (hRow ==0 && mRow ==0 && sRow ==0) {
        unit = @"ms";
        durationTime = [NSString stringWithFormat:@"%d %@",msRow, unit];
    }
    
    [roundDuration setText:durationTime];
    _exerciceTimeDuration = hRow*3600000 + mRow*60000 + sRow*1000 + msRow;
    
}

- (IBAction)breakTimeFieldSelected:(JVFloatLabeledTextField *)sender {
    
    NSLog(@"User selected %@", sender.text);
    int hRow = [[arrayOfHour objectAtIndex:[picker selectedRowInComponent:0]] intValue];
    int mRow = [[arrayOfMinute objectAtIndex:[picker selectedRowInComponent:1]] intValue];
    int sRow = [[arrayOfSecond objectAtIndex:[picker selectedRowInComponent:2]] intValue];
    int msRow = [[arrayOfMilliSecond objectAtIndex:[picker selectedRowInComponent:3]] intValue];
    NSString* unit;
    NSString* durationTime;
    
    if (hRow ==0) {
        unit = @"minutes(s)";
        durationTime = [NSString stringWithFormat:@"%d : %d : %d %@", mRow , sRow, msRow, unit];
    }else{
        unit = @"hour(s)";
        durationTime = [NSString stringWithFormat:@"%d : %d : %d : %d %@",hRow, mRow , sRow, msRow, unit];
    }
    if (hRow ==0 && mRow ==0) {
        unit = @"second(s)";
        durationTime = [NSString stringWithFormat:@"%d : %d %@",sRow, msRow, unit];
    }
    if (hRow ==0 && mRow ==0 && sRow ==0) {
        unit = @"ms";
        durationTime = [NSString stringWithFormat:@"%d %@",msRow, unit];
    }
    
    [breakDuration setText:durationTime];
    _breakTimeDuration = hRow*3600000 + mRow*60000 + sRow*1000 + msRow;
 }

- (IBAction)nbRepetitionFieldSelected:(JVFloatLabeledTextField *)sender {
    _exerciceNbRepeat = [sender.text intValue];
    NSLog (@"nbRepeat: %d  ", _exerciceNbRepeat);
}

-(void)done_clicked:(id)sender
{
    [roundDuration resignFirstResponder];
    [breakDuration resignFirstResponder];
    [roundNb resignFirstResponder];
}

-(void)cancel_clicked:(id)sender
{
    [roundDuration resignFirstResponder];
    [breakDuration resignFirstResponder];
    [roundNb resignFirstResponder];
    //roundNb.text = @"";
    
}



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 4;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 0)
        return arrayOfHour.count;
    if(component == 1)
        return arrayOfMinute.count;
    if(component == 2)
        return arrayOfSecond.count;
    if(component == 3)
        return arrayOfMilliSecond.count;
    
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *columnView = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, self.view.frame.size.width/3 - 35, 30)];
    columnView.text = [NSString stringWithFormat:@"%lu", row];
    columnView.textAlignment = NSTextAlignmentLeft;
    
    return columnView;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
     /*int hRow = [[arrayOfHour objectAtIndex:[picker selectedRowInComponent:0]] intValue];
    int mRow = [[arrayOfMinute objectAtIndex:[picker selectedRowInComponent:1]] intValue];
    int sRow = [[arrayOfSecond objectAtIndex:[picker selectedRowInComponent:2]] intValue];
    int msRow = [[arrayOfMilliSecond objectAtIndex:[picker selectedRowInComponent:3]] intValue];
    
   NSLog (@"%d Hours", hRow);
    NSLog (@"%d Minutes", mRow);
    NSLog (@"%d seconds", sRow);
    NSLog (@"%d ms", msRow);*/
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    ViewController* counterVC = [segue destinationViewController ];
    //counterVC.translatedText = self.translatedText;
    
    counterVC.nbRepeat = _exerciceNbRepeat;
    counterVC.exerciceTimeDuration = _exerciceTimeDuration;
    counterVC.breakTimeDuration = _breakTimeDuration;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
