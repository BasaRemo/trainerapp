//
//  TimeSelectionViewController.h
//  TrainingTimer
//
//  Created by Ntambwa Basambombo on 2014-09-21.
//  Copyright (c) 2014 Basa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "ESTimePicker.h"
#import "ActionSheetCustomPicker.h"
#import "ActionSheetStringPicker.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"
#import "VENSeparatorView.h"
#import "FlatUIKit.h"

@interface TimeSelectionViewController : UIViewController <UIPickerViewDelegate,UIPickerViewDataSource>{

}
@property int exerciceNbRepeat;
@property int exerciceTimeDuration;
@property int breakTimeDuration;
@property UIPickerView *picker;
@property VENSeparatorView* separatorView;
@property (weak, nonatomic) IBOutlet VENSeparatorView *separatorView2;
@property (weak, nonatomic) IBOutlet VENSeparatorView *separatorView3;
@property (weak, nonatomic) IBOutlet VENSeparatorView *separatorView4;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *roundNb;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *breakDuration;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *roundDuration;
- (IBAction)durationTextFieldPressed:(JVFloatLabeledTextField *)sender;

@property (weak, nonatomic) IBOutlet FUIButton *startTrainingButton;
- (IBAction)durationFieldSelected:(JVFloatLabeledTextField *)sender;
- (IBAction)breakTimeFieldSelected:(JVFloatLabeledTextField *)sender;
- (IBAction)nbRepetitionFieldSelected:(JVFloatLabeledTextField *)sender;

@end
