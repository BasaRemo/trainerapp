//
//  ViewController.h
//  TrainingTimer
//
//  Created by Ntambwa Basambombo on 2014-09-20.
//  Copyright (c) 2014 Basa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFRoundProgressCounterView.h"
#import "TWMessageBarManager.h"
#import "AFSoundManager.h"
#import "FlatUIKit.h"
#import "UserVoice.h"
#import "BFPaperButton.h"
#import "UIColor+BFPaperColors.h"

@interface ViewController : UIViewController <SFRoundProgressCounterViewDelegate,SFCounterLabelDelegate>{
}

@property int nbRepeat;
@property int exerciceTimeDuration;
@property int breakTimeDuration;
@property (weak, nonatomic) IBOutlet SFRoundProgressCounterView *counterViewSecond;
- (IBAction)startCountdown:(id)sender;
@property (strong, nonatomic) IBOutlet BFPaperButton *startStopButton;
@property (weak, nonatomic) IBOutlet BFPaperButton *resetBtn;
- (IBAction)resetTapped:(id)sender;


- (void)countdownDidEnd:(SFRoundProgressCounterView*)progressCounterView;
- (void)intervalDidEnd:(SFRoundProgressCounterView*)progressCounterView WithIntervalPosition:(int)position;
- (void)counter:(SFRoundProgressCounterView *)progressCounterView didReachValue:(unsigned long long)value;

@end
