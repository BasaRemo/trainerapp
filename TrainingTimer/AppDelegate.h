//
//  AppDelegate.h
//  TrainingTimer
//
//  Created by Ntambwa Basambombo on 2014-09-20.
//  Copyright (c) 2014 Basa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
