
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFSoundManager
#define COCOAPODS_POD_AVAILABLE_AFSoundManager
#define COCOAPODS_VERSION_MAJOR_AFSoundManager 1
#define COCOAPODS_VERSION_MINOR_AFSoundManager 2
#define COCOAPODS_VERSION_PATCH_AFSoundManager 3

// ActionSheetPicker-3.0
#define COCOAPODS_POD_AVAILABLE_ActionSheetPicker_3_0
#define COCOAPODS_VERSION_MAJOR_ActionSheetPicker_3_0 1
#define COCOAPODS_VERSION_MINOR_ActionSheetPicker_3_0 1
#define COCOAPODS_VERSION_PATCH_ActionSheetPicker_3_0 13

// BFPaperButton
#define COCOAPODS_POD_AVAILABLE_BFPaperButton
#define COCOAPODS_VERSION_MAJOR_BFPaperButton 1
#define COCOAPODS_VERSION_MINOR_BFPaperButton 5
#define COCOAPODS_VERSION_PATCH_BFPaperButton 3

// DZNEmptyDataSet
#define COCOAPODS_POD_AVAILABLE_DZNEmptyDataSet
#define COCOAPODS_VERSION_MAJOR_DZNEmptyDataSet 1
#define COCOAPODS_VERSION_MINOR_DZNEmptyDataSet 4
#define COCOAPODS_VERSION_PATCH_DZNEmptyDataSet 1

// ESTimePicker
#define COCOAPODS_POD_AVAILABLE_ESTimePicker
#define COCOAPODS_VERSION_MAJOR_ESTimePicker 0
#define COCOAPODS_VERSION_MINOR_ESTimePicker 4
#define COCOAPODS_VERSION_PATCH_ESTimePicker 0

// FlatUIKit
#define COCOAPODS_POD_AVAILABLE_FlatUIKit
#define COCOAPODS_VERSION_MAJOR_FlatUIKit 1
#define COCOAPODS_VERSION_MINOR_FlatUIKit 4
#define COCOAPODS_VERSION_PATCH_FlatUIKit 0

// JVFloatLabeledTextField
#define COCOAPODS_POD_AVAILABLE_JVFloatLabeledTextField
#define COCOAPODS_VERSION_MAJOR_JVFloatLabeledTextField 0
#define COCOAPODS_VERSION_MINOR_JVFloatLabeledTextField 0
#define COCOAPODS_VERSION_PATCH_JVFloatLabeledTextField 9

// SFCountdownView
#define COCOAPODS_POD_AVAILABLE_SFCountdownView
#define COCOAPODS_VERSION_MAJOR_SFCountdownView 0
#define COCOAPODS_VERSION_MINOR_SFCountdownView 0
#define COCOAPODS_VERSION_PATCH_SFCountdownView 1

// SFRoundProgressCounterView
#define COCOAPODS_POD_AVAILABLE_SFRoundProgressCounterView
#define COCOAPODS_VERSION_MAJOR_SFRoundProgressCounterView 0
#define COCOAPODS_VERSION_MINOR_SFRoundProgressCounterView 0
#define COCOAPODS_VERSION_PATCH_SFRoundProgressCounterView 5

// TTTAttributedLabel
#define COCOAPODS_POD_AVAILABLE_TTTAttributedLabel
#define COCOAPODS_VERSION_MAJOR_TTTAttributedLabel 1
#define COCOAPODS_VERSION_MINOR_TTTAttributedLabel 7
#define COCOAPODS_VERSION_PATCH_TTTAttributedLabel 5

// TWMessageBarManager
#define COCOAPODS_POD_AVAILABLE_TWMessageBarManager
#define COCOAPODS_VERSION_MAJOR_TWMessageBarManager 1
#define COCOAPODS_VERSION_MINOR_TWMessageBarManager 7
#define COCOAPODS_VERSION_PATCH_TWMessageBarManager 0

// UIColor+BFPaperColors
#define COCOAPODS_POD_AVAILABLE_UIColor_BFPaperColors
#define COCOAPODS_VERSION_MAJOR_UIColor_BFPaperColors 1
#define COCOAPODS_VERSION_MINOR_UIColor_BFPaperColors 2
#define COCOAPODS_VERSION_PATCH_UIColor_BFPaperColors 1

// VENSeparatorView
#define COCOAPODS_POD_AVAILABLE_VENSeparatorView
#define COCOAPODS_VERSION_MAJOR_VENSeparatorView 1
#define COCOAPODS_VERSION_MINOR_VENSeparatorView 0
#define COCOAPODS_VERSION_PATCH_VENSeparatorView 1

// XLForm
#define COCOAPODS_POD_AVAILABLE_XLForm
#define COCOAPODS_VERSION_MAJOR_XLForm 2
#define COCOAPODS_VERSION_MINOR_XLForm 0
#define COCOAPODS_VERSION_PATCH_XLForm 0

// uservoice-iphone-sdk
#define COCOAPODS_POD_AVAILABLE_uservoice_iphone_sdk
#define COCOAPODS_VERSION_MAJOR_uservoice_iphone_sdk 3
#define COCOAPODS_VERSION_MINOR_uservoice_iphone_sdk 2
#define COCOAPODS_VERSION_PATCH_uservoice_iphone_sdk 0

